# 一些不是很有用的东西
（都是自己机翻的东西，但还是根据具体情况修改，有的可能看起来很怪是因为我也不明白什么意思）

善用目录！











### Visual Novel Maker
 **原代码链接：** 

1. [语言配置文件](https://gitee.com/Frozen_Dlil/story-machine/blob/master/%E4%B8%80%E4%BA%9B%E6%96%87%E4%BB%B6/%E8%AF%AD%E8%A8%80%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6) 

 **文件下载：** 

[提取码：1234](https://pan.baidu.com/s/1lHBrd89HOJHrm7PrlmxAEQ )


 **说明：**  

1. 其实大多机翻后修改了一下意思，里面有的看起来很怪的翻译其实我也不是很明白这个功能，可以自行修改。
> **导入方式：请创建新语言，选中右键导入CSV，选中所在文件夹即可完成。** 



### RPG Maker MV Tools - Database ConVerter MV
![中文logo](%E4%B8%80%E4%BA%9B%E6%96%87%E4%BB%B6/logo_ch.png)
 **文件下载：** 

[查看更新](https://gitee.com/Frozen_Dlil/story-machine/commit/9ca88d0536fb0965eea9c46fbefd8eab13b10486)

[提取码：63h8](链接：https://pan.baidu.com/s/1jxo7ctPnFFlUhaZOnig0nQ#list/path=%2F)

[语言配置文件](https://gitee.com/Frozen_Dlil/story-machine/blob/master/%E4%B8%80%E4%BA%9B%E6%96%87%E4%BB%B6/translation.json)

**说明：**
1. 大多数为机翻修改了意思，也有参考了RPG make mv中的系统翻译，因为翻译的时候发现有些词是多意，所以选取了比较符合的翻译来。
2. 压缩包只添加了系统翻译与帮助页面，其中更改了默认语言。
3. 别吐槽，只是图个方便！


> 主要是添加了中文，使用后会直接将中文作为默认。
> 文件覆盖方式：所在盘\SteamLibrary\steamapps\common\RPG Maker MV Tools - Database ConVerter MV\resources\app 把压缩包里的文件直接甩进去。
> 换回英文或日文时，请修改（上面的地址）中的package.json文件里的倒数第二行【"language": "ch"】的语言简写，日文为jp、英文为en。
